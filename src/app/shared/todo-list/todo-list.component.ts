import { Todo, TodoList } from './../../models/todo.model';
import { TodoService } from './../../service/todo.service';
import { CreateTodoComponent } from './../create-todo/create-todo.component';
import { AuthService } from './../../service/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit, OnDestroy {

  name = "";
  todos: TodoList[] = [];
  userTodo?: TodoList;
  todo: Todo[] = [];

  private usernameListenerSubs?: Subscription;
  private todoListenerSubs?: Subscription;

  constructor(private authService: AuthService, private dialog: MatDialog, private todoService: TodoService) { }

  ngOnInit(): void {
    this.name = this.authService.getUsername();
    this.usernameListenerSubs = this.authService.getUsernameStatusListener().subscribe(username => this.name = username);
    this.todos = this.todoService.getAllTodo()!;
    this.todoListenerSubs = this.todoService.getAllTodoUpdateListener()
    .subscribe((postData: {todos: TodoList[]}) => this.todos = postData.todos);
    this.userTodo = this.todos?.filter(td => td.name.toLowerCase() === this.name.toLowerCase())[0];
    this.todo = this.userTodo.todos;
  }

  openCreateTodoDialog(): void {
    const dialogRef = this.dialog.open(CreateTodoComponent, {
      width: '600px',
      data: this.name
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('The dialog was closed');
      }
    });
  }

  ngOnDestroy(): void {
    if (this.usernameListenerSubs) {
      this.usernameListenerSubs.unsubscribe();
    }

    if (this.todoListenerSubs) {
      this.todoListenerSubs.unsubscribe();
    }
  }

}
