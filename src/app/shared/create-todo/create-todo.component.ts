import { TodoService } from './../../service/todo.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.scss']
})
export class CreateTodoComponent implements OnInit {

  titleFormControl = new FormControl('', [Validators.required, Validators.minLength(5)]);
  dueDateFormControl = new FormControl('', [Validators.required, Validators.minLength(5)]);
  dueDateTextFormControl = new FormControl('', [Validators.required, Validators.minLength(5)]);
  minDate: moment.Moment = moment().add(1, 'day');

  constructor(private dialogRef: MatDialogRef<CreateTodoComponent>, private todoService: TodoService,
            @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSaveTodo(): void {
    const title = this.titleFormControl.value
    const dueDate = this.dueDateTextFormControl.value
    if (title.length < 5 || dueDate.length < 5) {
      return console.log('empty ini form');
    }
    console.log(this.data, title, dueDate);
    this.todoService.addTodo(title, dueDate, this.data);
    this.dialogRef.close();
  }

  onDateChange(): void {
    const currentDte = this.dueDateFormControl.value;
    const formated = moment(currentDte).format("DD MMMM YYYY hh:mmA");
    this.dueDateTextFormControl.setValue(formated);
  }

}
