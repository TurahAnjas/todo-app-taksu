import { AuthService } from './../../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  nameFormControl = new FormControl('', [Validators.required, Validators.minLength(5)]);

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if (this.nameFormControl.value.length < 5) {
      return console.log('empty ini');
    }
    console.log(this.nameFormControl.value)
    const name = this.nameFormControl.value;
    this.authService.login(name);
  }

}
