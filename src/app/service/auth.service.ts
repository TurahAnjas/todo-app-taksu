import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authStatusListener = new Subject<boolean>();
  private usernameStatusListener = new Subject<string>();
  private username = "";
  private isLogin = false;

  constructor() { }

  getIsLogin() {
    return this.isLogin;
  }

  getUsername() {
    return this.username;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getUsernameStatusListener() {
    return this.usernameStatusListener.asObservable();
  }

  login(username: string) {
    this.isLogin = true;
    this.username = username;
    this.authStatusListener.next(true);
    this.usernameStatusListener.next(username);
  }
}
