import { Todo, TodoList } from './../models/todo.model';
import { Injectable } from '@angular/core';
import { map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private todo?: TodoList;
  private allTodos?: TodoList[] = [];
  private AllTodoUpdate = new Subject<{todos: TodoList[]}>();

  constructor() { }

  getAllTodoUpdateListener() {
    return this.AllTodoUpdate.asObservable();
  }

  addTodo(title: string, dueDate: string, username: string) {
    console.log(this.allTodos);
    console.log(title, dueDate, username);
    const newTodo: Todo = {
      id: Date.now(),
      task: title,
      dueDate,
      isDone: false
    }
    if (this.allTodos?.length === 0 || !this.allTodos) {
      const newUser: TodoList = {
        id: Date.now(),
        name: username,
        todos: [newTodo]
      }
      this.todo = {...newUser};
      this.allTodos = [this.todo];
      console.log(this.todo, this.allTodos);
      localStorage.setItem('todos', JSON.stringify(this.allTodos));
      this.AllTodoUpdate.next({todos: this.allTodos});
      return;
    }
    const todos = this.allTodos?.filter(td => td.name.toLowerCase() === username.toLowerCase())[0];
    const oldTodos = todos.todos;
    todos.todos = [...oldTodos, newTodo]
  }

  getAllTodo() {
    this.allTodos = JSON.parse(localStorage.getItem('todos')!);
    this.AllTodoUpdate.next({todos: this.allTodos!});
    return this.allTodos;
  }
}
