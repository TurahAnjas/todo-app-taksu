import { AuthService } from './../../service/auth.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit, OnDestroy {

  isLogin = false;

  private authListenerSubs?: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isLogin = this.authService.getIsLogin();
    this.authListenerSubs = this.authService.getAuthStatusListener().subscribe(auth => this.isLogin = auth);
  }

  ngOnDestroy(): void {
    if (this.authListenerSubs) {
      this.authListenerSubs.unsubscribe();
    }
  }

}
