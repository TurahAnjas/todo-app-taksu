export interface Todo {
  id: number;
  task: string;
  dueDate: string;
  isDone: boolean;
}

export interface TodoList {
  id: number;
  name: string;
  todos: Todo[];
}
