# TodoApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.3. This project is used to add new todo and update progress of existing todo. This project can be used for multiple user.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Next Features

1. Update progress status for individual Todo
2. Delete individual Todo
3. Save Todo on DB
